<?php
//incluye la clase Libro y CrudLibro
require_once('../logica_negocios/LibroLN.php');
require_once('../clases_negocios/LibroCN.php');
$crud=new LibroLN();
$libro= new LibroCN();
$libro_edit= new LibroCN();
$accion = "insertar";
//obtiene todos los libros con el método mostrar de la clase crud
$listaLibros=$crud->mostrar();
if (isset($_GET['id'])){
    $libro_edit=$crud->obtenerLibro($_GET['id']);
	$accion = "actualizar";
}
?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
	 <meta charset="UTF-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <title>Mostrar Libros</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
	<link href="../css/estilos_.css"  rel="stylesheet">
	<link href="../css/estilos_mostrar_libros.css"  rel="stylesheet">
	<script src="../js/bootstrap.min.js" ></script>
 </head>
 <body>
 <div class="contenedor" >
	<div class="titulo div">Gestor de libros</div>
	<div class="derecho div">	
		<div class="div_scroll">
			<table  class="table table-hover">
				<thead  class="table-dark"> 
					<tr>
					<th scope="col">Nombre</th>
					<th scope="col">Autor</th>
					<th scope="col">Edicion</th>
					<th scope="col">Actualizar</th>
					<th scope="col">Eliminar</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($listaLibros as $libro) {?>
					<tr >
						<td scope="row" ><?php echo $libro->getNombre() ?></td>
						<td><?php echo $libro->getAutor() ?></td>
						<td><?php echo $libro->getAnio_edicion()?> </td>
						<!-- <td><a class="positivo" href="actualizar.php?id=<?php echo $libro->getId()?>&accion=a">Actualizar</a> </td> -->
						<td><a class="positivo" href="mostrar.php?id=<?php echo $libro->getId()?>&accion=modificar">Actualizar</a> </td>
						<td><a class="negativo"  href="../gestor_logico/libroGL.php?id=<?php echo $libro->getId()?>&accion=e">Eliminar</a>   </td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="izquierdo div">
		<form action='../gestor_logico/LibroGL.php' method='post' style="width: 70%;">
			<table>
				<tr>
					<div class="form-group">
						<label for="nombre_libro" style="background: black; width: 100%; color: white;">Datos de libro</label>
					</div>
					<input type='hidden' name='id' value='<?php echo $libro_edit->getId()?>'>
					<div class="form-group">
						<label for="nombre_libro">Nombre libro:</label>
						<input type="text" class="form-control" id="nombre_libro" placeholder="Ingrese Nombre" name='nombre' value='<?php echo $libro_edit->getNombre()?>'>
						<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					</div>
					<br>
					<div class="form-group">
						<label for="Autor">Autor:</label>
						<input type="text" class="form-control" id="Autor" placeholder="Ingrese Autor" name='autor' value='<?php echo $libro_edit->getAutor()?>'>
						<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					</div>
					<br>
					<div class="form-group">
						<label for="fecha">Fecha Edición::</label>
						<input type="text" class="form-control" id="fecha" placeholder="Fecha Edición" name='edicion' value='<?php echo $libro_edit->getAnio_edicion()?>'>
						<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					</div>
					<input type='hidden' name='<?php echo $accion?>' value='insertar'>
					
					
				</tr>
				
			</table>
			<br>

			<input class="btn_positivo"  type='submit' value='Guardar'>
			<input class="btn_negativo"  type='button' value='Volver'  onclick="location.href='index.php';">
			<!-- <a class="btn_negativo" href="index.php">Volver</a> -->
		</form>
	</div>
	<div class="pie div">aartback ♥</div>
		
	</div>


	
 </body>
 </html>
