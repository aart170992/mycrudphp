
<?php
//incluye la clase Libro y CrudLibro
	
	require_once('../logica_negocios/LibroLN.php');
	require_once('../clases_negocios/LibroCN.php');
	$crud= new LibroLN();
	$libro=new LibroCN();
	//busca el libro utilizando el id, que es enviado por GET desde la vista mostrar.php
	$libro=$crud->obtenerLibro($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Actualizar Libro</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<script src="../js/bootstrap.min.js" ></script>
	<link href="../css/estilos_modificar_libro.css"  rel="stylesheet">

</head>
<body>
<form action='../gestor_logico/LibroGL.php' method='post'>
	<!-- <form action='administrar_libro.php' method='post'> -->
	<div class="contenedor" >
	<div class="titulo">	asd</div>
	<div class="derecho">	
	<table>
		<tr>
			<input type='hidden' name='id' value='<?php echo $libro->getId()?>'>
			<td>Nombre libro:</td>
			<td> <input type='text' name='nombre' value='<?php echo $libro->getNombre()?>'></td>
		</tr>
		<tr>
			<td>Autor:</td>
			<td><input type='text' name='autor' value='<?php echo $libro->getAutor()?>' ></td>
		</tr>
		<tr>
			<td>Fecha Edición:</td>
			<td><input type='text' name='edicion' value='<?php echo $libro->getAnio_edicion() ?>'></td>
		</tr>
		<input type='hidden' name='actualizar' value'actualizar'>
	</table>
	<input type='submit' value='Guardar'>
	<a href="index.php">Volver</a>

	</div>
	<div class="izquierdo">	</div>
	<div class="pie">	</div>
		
	</div>
	
</form>
</body>
</html>


